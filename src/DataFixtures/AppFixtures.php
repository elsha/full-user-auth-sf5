<?php

namespace App\DataFixtures;

use App\Entity\Rule;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class AppFixtures
 * @package App\DataFixtures
 */
class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * AppFixtures constructor.
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->loadRules($manager);
        $this->loadUsers($manager);
    }

    /**
     * @param ObjectManager $manager
     */
    private function loadRules(ObjectManager $manager)
    {
        $role1 = new Rule();
        $role1->setName('SUPER ADMINISTRATEUR');
        $role1->setAbreviation('SUPER_ADMIN');
        $role1->setRightToken('ROLE_SUPER_ADMIN');
        $role1->setDescription('');
        $role1->setOrderNumber(1);
        $manager->persist($role1);


        $role2 = new Rule();
        $role2->setName('ADMINISTRATEUR');
        $role2->setAbreviation('ADMIN');
        $role2->setRightToken('ROLE_ADMIN');
        $role2->setDescription('');
        $role2->setOrderNumber(2);
        $manager->persist($role2);

        $role3 = new Rule();
        $role3->setName('UTILISATEUR');
        $role3->setAbreviation('USER');
        $role3->setRightToken('ROLE_USER');
        $role3->setDescription('');
        $role3->setOrderNumber(3);
        $manager->persist($role3);

        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     */
    private function loadUsers(ObjectManager $manager)
    {
        foreach ($this->getUserData() as [$full_name, $username, $password, $email, $roles, $idRule]) {
            $user = new User();
            $user->setName($full_name);
            $user->setUsername($username);
            $user->setPassword($this->passwordEncoder->encodePassword($user, $password));
            $user->setEmail($email);
            $user->setRoles($roles);
            $user->setRule($manager->getRepository(Rule::class)->findOneBy(['id' => $idRule], ['id' => 'ASC']));

            $manager->persist($user);
            $this->addReference($username, $user);
        }
        $manager->flush();
    }

    /**
     * @return array
     */
    private function getUserData(): array
    {
        // $userData = [$full_name, $username, $password, $email, $roles];
        return [
            ['Super Admin', 'super_admin', 'SAdmin0000', 'super_admin@app.com', ['ROLE_USER', 'ROLE_SUPER_ADMIN', 'ROLE_ADMIN'], 1],
            ['Admin', 'admin', 'Admin0000', 'admin@app.com', ['ROLE_USER', 'ROLE_ADMIN'], 2],
            ['User', 'user', 'User0000', 'user@app.com', ['ROLE_USER'], 3]
        ];
    }
}
