<?php

// Change the namespace according to the location of this class in your bundle
namespace App\EventListener;

use App\Entity\User;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class LoginListener
{
    /**
     * @var FlashyNotifier
     */
    private $notifier;

    public function __construct(FlashyNotifier $notifier)
    {
        $this->notifier = $notifier;
    }

    /**
     * @param InteractiveLoginEvent $event
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        /* @var $user User */
        $user = $event->getAuthenticationToken()->getUser();
        $this->notifier->success('Welcome '.$user->getFullName());
    }
}