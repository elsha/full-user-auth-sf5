<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Class DefaultController
 * @package App\Controller
 */
class DefaultController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * DefaultController constructor.
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route("/",  name="home", methods={"GET"})
     * @return Response
     */
    public function userHome(): Response
    {
        if ($this->getUser()){
            return $this->redirectToRoute('admin_home');
        }
        return $this->render('public/index.html.twig');
    }

    /**
     * @Route("/admin", name="admin_home")
     * @return Response
     */
    public function adminHome()
    {
        return $this->render('admin/index.html.twig');
    }

    /**
     * @Route("/redirect", name="homepage_redirect", methods={"GET"})
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return Response
     */
    public function redirectRoute(AuthorizationCheckerInterface $authorizationChecker)
    {
        if (true === $authorizationChecker->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('admin_home');
        } else {
            return $this->redirectToRoute('home');
        }
    }
}