<?php

namespace App\Controller\Auth;

use ApiPlatform\Core\Api\UrlGeneratorInterface;
use App\Entity\Rule;
use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Form\ResetType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

/**
 * Class RegistrationController
 * @package App\Controller\Auth
 */
class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param \Swift_Mailer $mailer
     * @return Response
     * @internal param GuardAuthenticatorHandler $guardHandler
     * @internal param UsersAuthenticator $authenticator
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, \Swift_Mailer $mailer): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword($passwordEncoder->encodePassword($user, $form->get('plainPassword')->getData()));
            //            One generate the activation token
            $user->setActivationToken(md5(uniqid()));
            $entityManager = $this->getDoctrine()->getManager();
            $rule = $entityManager->getRepository(Rule::class)->find(3);
            $user->setRule($rule);
            $entityManager->persist($user);
            $entityManager->flush();

            // do anything else you need here, like send an email
            $message = (new \Swift_Message('Activation of account'))
                // On attribue l'expediteur
                ->setFrom('no-reply@greensoft-group.com')
                // On attribue le destinataire
                ->setTo($user->getEmail())
                // On crée le contenu
                ->setBody($this->renderView('auth/activation/email.html.twig', ['token' => $user->getActivationToken()]), 'text/html');

            // On envoie le mail
            $mailer->send($message);

            $this->addFlash('success', 'Your account has been created successfully, Check your email to activate the account');
            return $this->redirectToRoute('app_login');
        }

        return $this->render('auth/registration/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/activation/{token}", name="activation")
     * @param $token
     * @param UserRepository $repository
     * @return RedirectResponse
     */
    public function activation($token, UserRepository $repository)
    {
        //On verifie si un utilisateur a ce token
        $user = $repository->findOneBy(['activation_token' => $token]);

        //Si aucun utilisateur n'existe avec ce token
        if (!$user) {
            throw $this->createNotFoundException('This user does not exist');
        }
        // We delete the token
        $user->setActivationToken(null);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        // sent a flash message
        $this->addFlash('success', 'Your account has been activated');
        return $this->redirectToRoute('app_login');
    }

    /**
     * @Route("/password/forgotten", name="app_password_forgotten")
     * @param Request $request
     * @param UserRepository $repository
     * @param \Swift_Mailer $mailer
     * @param TokenGeneratorInterface $tokenGenerator
     * @return RedirectResponse|Response
     */
    public function forgottenPassword(Request $request, UserRepository $repository, \Swift_Mailer $mailer, TokenGeneratorInterface $tokenGenerator)
    {
        $form = $this->createForm(ResetType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            /* @var $user User */
            $user = $repository->findOneBy(['email' => $data['email']]);

            if (!$user) {
                $this->addFlash('warning', 'This user does not exist');
                return $this->redirectToRoute('app_login');
            }

            $token = $tokenGenerator->generateToken();
            try {
                $user->setResetToken($token);
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
            } catch (\Exception $exception) {
                $this->addFlash('warning', 'An error occur during the request, Contact the platform administrator if it persists' . $exception->getMessage());
                return $this->redirectToRoute('app_login');
            }
            $url = $this->generateUrl('app_reset_password', ['token' => $token], UrlGeneratorInterface::ABS_URL);

            $message = (new \Swift_Message('Forgotten password'))
                ->setFrom('no-reply@greensoft-group.com')
                ->setTo($user->getEmail())
                ->setBody("<p>Good morning, </p><p> A password reinitialisation has done for the app GSGT-Auth. Click on this link: " . $url . "</p>", 'text/html');
            $mailer->send($message);
            $this->addFlash('success', 'A reinitialisation email has been sent ');
            return $this->redirectToRoute('app_login');
        }
        return $this->render('auth/security/forgotten_password.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("app/reset/{token}/password", name="app_reset_password")
     * @param $token
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return RedirectResponse|Response
     */
    public function resetPassword($token, Request $request, UserPasswordEncoderInterface $encoder)
    {
        $user =  $this->getDoctrine()->getManager()->getRepository(User::class)->findOneBy(['reset_token' => $token]);
        if (!$user) {
            $this->addFlash('warning', 'Incorrect token provided');
            return $this->redirectToRoute('app_login');
        }
        if ($request->isMethod('POST')) {
            $user->setResetToken(null);

            $user->setPassword($encoder->encodePassword($user, $request->request->get('password')));
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', 'Password updated successfully');
            return $this->redirectToRoute('app_login');
        }
        return $this->render('auth/security/reset_password.html.twig', [
            'token' => $token
        ]);
    }
}
