<?php

namespace App\Service;

use Spipu\Html2Pdf\Html2Pdf;

/**
 * Class Html2PdfService
 * @package App\Service
 */
class Html2PdfService
{
    /**
     * @var
     */
    private $pdf;

    /**
     * @var
     */
    private $targetDir;

    /**
     * @var
     */
    private $publicDir;

    /**
     * Html2PdfService constructor.
     * @param $targetDir
     * @param $publicDir
     */
    public function __construct($targetDir, $publicDir)
    {
        $this->targetDir = $targetDir;
        $this->publicDir = $publicDir;
    }

    /**
     * @param null $orientation
     * @param null $format
     * @param null $lang
     * @param null $unicode
     * @param null $encoding
     * @param null $margin
     */
    public function create ($orientation = null, $format = null, $lang = null, $unicode = null, $encoding = null, $margin = null)
    {
        $this->pdf = new Html2Pdf(
            $orientation ? $orientation : $this->orientation,
            $format ? $format : $this->format,
            $lang ? $lang : $this->lang,
            $unicode ? $unicode : $this->unicode,
            $encoding ? $encoding : $this->encoding,
            $margin ? $margin : $this->margin
        );
    }


    /**
     * @param $template
     * @param $name
     * @param null $directoryName
     * @param $title
     * @param $destination
     * @return mixed
     */
    public function generatePdf($template, $name, $directoryName = NULL, $title, $destination)
    {
        $this->pdf->writeHTML($template);
        $this->pdf->pdf->SetTitle($title);
        return $this->pdf->Output($this->getTargetDir().'/'.$directoryName.'/'.$name, $destination);
    }

    /**
     * @param $template
     * @param $title
     * @return mixed
     */
    public function generatePdfSimple($template, $title)
    {
        $this->pdf->writeHTML($template);
        $this->pdf->pdf->SetTitle($title);
        return $this->pdf->output($title);
    }

    /**
     * @return mixed
     */
    public function getTargetDir()
    {
        return $this->targetDir;
    }

    /**
     * @return mixed
     */
    public function getPublicDir()
    {
        return $this->publicDir;
    }

    /**
     * @param mixed $publicDir
     */
    public function setPublicDir($publicDir)
    {
        $this->publicDir = $publicDir;
    }
}