<?php

// App/Service/EmailSender.php
namespace App\Service;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class EmailSender
 * @package App\Service
 */
class EmailSender extends AbstractController
{
    // Envoie generique d'un email
    /**
     * @param \Swift_Mailer $mailer
     * @param $template
     * @param User $user
     * @param null $sender
     * @param null $url
     * @param null $subject
     * @param null $text
     * @param null $attachment1
     * @param null $attachment2
     */
    public function sendMail(\Swift_Mailer $mailer, $template, User $user, $sender = null, $url = null, $subject = null, $text = null, $attachment1 = null, $attachment2 = null)
    {
        //Initialize the message
        $message = (new \Swift_Message($subject != null ? $subject : 'Greetings'))
            ->setFrom('no-reply@greensoft-group.com')
            /* @var $user User */
            ->setTo($user->getEmail())
            ->setCharset('utf-8')
            ->setContentType('text/html')
            ->setBody($this->renderView($template, [
                'user' => $user,
                'message' => $text
            ]));
            if ($attachment1){
                $message->attach(\Swift_Attachment::fromPath($attachment1));
            }
            if ($attachment2){
                $message->attach(\Swift_Attachment::fromPath($attachment2));
            }
        //Send the message
        $mailer->send($message);
    }
}