<?php

// App/Service/DompdfService.php
namespace App\Service;

use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class DompdfService
 * @package App\Service
 */
class DompdfService
{
    /**
     * @var
     */
    private $targetDir;

    /**
     * DompdfService constructor.
     * @param $targetDir
     */
    public function __construct($targetDir = null)
    {
        $this->targetDir = $targetDir;
    }

    /**
     * @param $template //Template of the pdf to generate
     * @param null $title //title of the file
     * @param null $directoryName // Directory name of the file if you want to save
     * @param string $size
     * @param string $orientation
     * @param string $action // how to display the output, In a browser, force the download, save the file on a disk
     * @return string
     */
    public function upload($template, $title, $directoryName = null, $size = 'A4', $orientation = 'portrait', $action ="O")
    {
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Load HTML to Dompdf
        $dompdf->loadHtml($template);

        // (Optional) Setup the paper size and orientation 'portrait' or 'landscape'
        $dompdf->setPaper($size, $orientation);

        // Render the HTML as PDF
        $dompdf->render();

        if ($action == "D"){ // generate the document and force the download
            // Output the generated PDF to Browser (force download)
            $dompdf->stream($title . '.pdf', [
                "Attachment" => true
            ]);
        } elseif ($action == "O"){ // generate and display in the browser
            /// Output the generated PDF to Browser (inline view)
            $dompdf->stream($title . '.pdf', [
                "Attachment" => false
            ]);
        }else{ //Save the file on the disk
            // Store PDF Binary Data
            $output = $dompdf->output();
            $pdfFilepath = $this->targetDir . '/'.$directoryName.'/' . $title . '.pdf';

            // Write file to the desired path
            file_put_contents($pdfFilepath, $output);
        }
        return ;
    }

    /**
    * @param string $fileName
    * @return void
    */
    public function removeFile($fileName)
    {
        $filesystem = new Filesystem();
        $filesystem->remove($fileName);
    }

    /**
     * @return string
     */
    public function getTargetDir()
    {
        return $this->targetDir;
    }
}