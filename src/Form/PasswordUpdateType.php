<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PasswordUpdateType
 * @package App\Form
 */
class PasswordUpdateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('oldPassword', PasswordType::class, [
                'label' => 'Old password',
                'attr' => [
                    'placeholder' => 'Enter your current password...'
                ]
            ])
            ->add('newPassword', PasswordType::class, [
                'label' => 'New password',
                'attr' => [
                    'placeholder' => 'Enter your new password...'
                ]
            ])
            ->add('confirmPassword', PasswordType::class, [
                'label' => 'Confirm New password',
                'attr' => [
                    'placeholder' => 'Confirm your new password...'
                ]
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
