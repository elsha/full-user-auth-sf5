Full Users Authentication for Symfony 5
========================

Welcome to the full user authentication for symfony 5, a fully-user
functional Symfony application that you can use as the base of
your web project.

Official documention: http://symfony.com/doc/current/index.html

Exigences
------------

  * PHP 7.2.5 or more;
  * composer 1.6.3 or more
  * node 9.6.1 or more
  * yarn 1.3.2 or more
  * npm 5.6.0 or more
  * extension PDO-SQLite PHP should be activated;
  * and others [specifics requirements for symfony 5][1].
  
  Installation
  ------------
  * Clone the project via this command line 
  ```bash
  $ git clone  https://gitlab.com/elsha/full-user-auth-sf5.git name_project
  ```
  
  * Install the dependancies
  ```bash
  $ cd name_project
  $ composer install
  ```
  
  * Intall node dependancies 
   ```bash
   $ npm install
   ```
   or 
```bash
  $ yarn install
```

How to use
-----
* Launch the application

 ```bash
 $ php -S 127.0.0.1:8000 -t public
 ```
 or
  ```bash
    $ symfony serve
```
if symfony is install on your machine

You can use Webpack to manage your CSS and Javascripts files. For more details on
the topic, This is the [official documentation](https://webpack.js.org/concepts/) or
watch this tutorial (https://www.youtube.com/watch?v=_KXGVca8uXw&list=PLjwdMgw5TTLVzGXGxEBdjwHXCeYnBb7n8&index=1)
which explain Webpack and how it is used

 ![](https://raw.githubusercontent.com/github/explore/6c6508f34230f0ac0d49e847a326429eefbfc030/topics/webpack/webpack.png)
 * Compile CSS and javacripts files using  **Webpack**
 ```bash
   $ npm run dev
   ```
   
   Create a file **.env** at the root of the project from **.env.dist** situated
   in the root of the project.
   
   If you want to use Sqlite for your project
   uncomment the line `#DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7` by removing the #
   and replace the hole line with `DATABASE_URL=sqlite:///%kernel.project_dir%/var/data/blog.sqlite`.<br>
    **blog.sqlite** is the name of the database. You can name it as you want.<br>
Otherwise if you want to user the mysql as your database manager  uncomment the same line and 
replace db_user with the database user, db_password: the password of the database, and db_name with the name
of the database.

Create the database with this command
 ```bash
   $ php bin/console doctrine:database:create
   ```
To update the database we have two options:<br>
Option 1: Migrations: Creating the Database Tables/Schema
 ```bash
   $  php bin/console make:migration
   ```
 And update the database
  ```bash
    $ php bin/console doctrine:migrations:migrate
  ```
  Option 2:
  Update the database without making the migration
   ```bash
     $ php bin/console doctrine:schema:update --force
   ```
   or
   ```bash
    $ php bin/console d:s:u --force
  ```
  
  Load the test data for a first use
  ```bash
   $ php bin/console doctrine:fixtures:load
 ```
 
 Open the project in your favorite browser via this link http://localhost:8000
 
<b>Administration Access via this link: http://localhost:8000/login</b><br>
username or email: admin or admin@app.com<br>
Password: Admin0000